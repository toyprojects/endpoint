package nl.dutchland.endpoint

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EndpointApplication

fun main(args: Array<String>) {
	runApplication<EndpointApplication>(*args)
}
